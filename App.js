import React, { Component } from 'react';
import { View, Text } from 'react-native';
import firebase from 'react-native-firebase';
import { createDrawerNavigator } from 'react-navigation-drawer';
import {createAppContainer} from 'react-navigation';
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  adduser(){
    firebase.auth().createUserWithEmailAndPassword('admin@gmail.com','Admin@1313')
    .then(() => {
      alert('User Created');
    })
    .catch((err) => {
      alert(err);
    })
  }
  addData(){
    firebase.firestore().collection('todos').add({
      testing:'testing'
    })
  }
  componentDidMount(){
    this.addData();
  }
  render() {
    return (
      <View>
        <Text> App </Text>
      </View>
    );
  }
}
const nav = createDrawerNavigator({
  App
})
export default createAppContainer(nav);
